﻿using TMPro;
using UnityEngine;

public class LeaderboardItem : MonoBehaviour
{
    private const string ZERO = "0";
    
    [SerializeField] private TextMeshProUGUI _nameText;
    [SerializeField] private TextMeshProUGUI _coinsText;

    public void SetNameAndCoins(string runnerName, ushort coins, Color color)
    {
        _nameText.color = color;
        
        _nameText.text = runnerName;
        _coinsText.text = coins.ToString();
    }

    public void ResetItem()
    {
        _nameText.text = string.Empty;
        _coinsText.text = ZERO;
    }
}
