﻿using System.Linq;
using MightyAttributes;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(menuName = "Model/Biome Model", fileName = "BiomeModel")]
public class BiomeModel : BaseModel
{
    [SerializeField, ValueFrom("GetLevels"), Reorderable(false, options: ArrayOption.DontFold | ArrayOption.DisableSizeField)]
    private LevelModel[] _levels;

    public void NextLevel()
    {
        var index = SavedDataServices.LevelIndex;
        if (IsLevelLoaded(index)) UnloadLevel(index);

        if (++index >= _levels.Length)
            InstanceManager.GameManager.NextBiome();
        else
            LoadLevel(index);
    }

    public void LoadLevel(byte index, bool saveProgression = true)
    {
        InstanceManager.GameManager.Pause();
        
        if (IsLevelLoaded(index)) UnloadLevel(index);

        SceneManager.LoadScene(_levels[index].SceneIndex, LoadSceneMode.Additive);
        
        if (saveProgression) SavedDataServices.LevelIndex = index;
    }

    public void UnloadLevel(byte index) => SceneManager.UnloadSceneAsync(_levels[index].SceneIndex);

    public bool IsLevelLoaded(byte index) => SceneManager.GetSceneByBuildIndex(_levels[index].SceneIndex).isLoaded;

    public void RestartLevel() => LoadLevel(SavedDataServices.LevelIndex, false);

#if UNITY_EDITOR
    private LevelModel[] GetLevels() => ReferencesUtilities.FindAssetsOfType<LevelModel>().Where(l => l.Biome == this).ToArray();
#endif
}