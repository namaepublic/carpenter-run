﻿using MightyAttributes;
using UnityEngine;

[CreateAssetMenu(menuName = "Model/Level Model", fileName = "LevelModel")]
public class LevelModel : BaseModel
{
    [SerializeField, SceneDropdown] private byte _sceneIndex;

    [SerializeField] private BiomeModel _biome;

    public byte SceneIndex => _sceneIndex;
    public BiomeModel Biome => _biome;
}