﻿public static class StaticLayers
{
    public const int TRACK_LAYER = 8;
    public const int WALKABLE_BOARD_LAYER = 9;
    public const int COLLECTIBLE_BOARD_LAYER = 10;
    public const int BOT_LAYER = 11;
    public const int PLAYER_LAYER = 12;
    public const int SHORTCUT_LAYER = 13;
    public const int FINISH_LINE_LAYER = 14;

    public const int CLIMB_DETECTION_MASK = 1 << TRACK_LAYER | 1 << WALKABLE_BOARD_LAYER;
    public const int TRACK_DETECTION_MASK = CLIMB_DETECTION_MASK | 1 << COLLECTIBLE_BOARD_LAYER | 1 << BOT_LAYER | 1 << FINISH_LINE_LAYER;
    public const int TARGET_DETECTION_MASK = 1 << SHORTCUT_LAYER | 1 << COLLECTIBLE_BOARD_LAYER;
}