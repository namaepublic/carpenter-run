﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using MightyAttributes;
using UnityEngine;


public class ShortcutBehaviour : BaseTargetBehaviour
{
    [SerializeField] private Transform _destinationTransform;
    [SerializeField, MinValue(0)] private float _destinationRadius;
    [SerializeField, MinValue(0)] private int _boardsNeeded;
    [SerializeField, PercentSlider] private int _chanceToCross;

    private Vector3 m_destination;

    public override void Init()
    {
        base.Init();
        m_destination = _destinationTransform.position;
    }

    public bool TryGetDestination(int boardAmount, int chance, out Vector3 destination)
    {
        if (CanCross(boardAmount, chance))
        {
            destination = GetDestination();
            return true;
        }

        destination = Vector3.zero;
        return false;
    }

    private bool CanCross(int boardAmount, int chance) => 
        boardAmount >= _boardsNeeded && Random.Range(0, 101) <= _chanceToCross && Random.Range(0, 101) <= chance;

    private Vector3 GetDestination()
    {
        var destinationCircle = Random.insideUnitCircle * _destinationRadius;
        return new Vector3(destinationCircle.x + m_destination.x, 0, destinationCircle.y + m_destination.z);
    }

#if UNITY_EDITOR
    private Mesh m_boardMesh;
    private const float BOARD_DISTANCE = .6f;
    private const float MAX_BOT_DISTANCE = 4;

    private float m_distanceFromGap;
    private float m_botDistance;
    private float m_destinationOffset;

    [Title("Debug")]
    [OnInspectorGUI]
    private void OnInspectorGUI()
    {
        EditorGUI.BeginChangeCheck();

        m_distanceFromGap = EditorGUILayout.FloatField("Distance From Gap", m_distanceFromGap);
        m_botDistance = EditorGUILayout.Slider("Bot Distance", m_botDistance, -MAX_BOT_DISTANCE, MAX_BOT_DISTANCE);
        m_destinationOffset = EditorGUILayout.Slider("Destination Offset", m_destinationOffset, -_destinationRadius, _destinationRadius);

        if (EditorGUI.EndChangeCheck())
            SceneView.RepaintAll();
    }

    private void OnDrawGizmos()
    {
        if (!_destinationTransform) return;
        if (!(GetComponentInChildren<SphereCollider>() is SphereCollider sphereCollider)) return;

        m_destination = _destinationTransform.position;

        var tfm = sphereCollider.transform;

        var selectedObjects = Selection.gameObjects;
        if (selectedObjects.Length == 0) return;

        var validObject = false;
        var showCollider = true;
        foreach (var selectObject in selectedObjects)
        {
            if (selectObject == _destinationTransform.gameObject || selectObject.GetComponent<BotBehaviour>())
            {
                validObject = true;
                continue;
            }

            if (selectObject != gameObject && selectObject != tfm.gameObject) continue;

            validObject = true;
            showCollider = false;
            break;
        }

        if (!validObject) return;

        var colliderRadius = sphereCollider.radius;

        var position = tfm.position;

        var direction = (m_destination - position).normalized;

        var crossDirection2D = Vector2.Perpendicular(new Vector2(direction.x, direction.z)).normalized;
        var crossDirection3D = new Vector3(crossDirection2D.x, 0, crossDirection2D.y).normalized;

        var offsetDestination = m_destination + crossDirection3D * m_destinationOffset;
        direction = (offsetDestination - position).normalized;

        var offsetGap = direction * m_distanceFromGap;
        var offsetBotDistance = crossDirection3D * m_botDistance;
        var offsetPosition = new Vector3(position.x, 0, position.z) + offsetGap + offsetBotDistance;

        direction = (offsetDestination - offsetPosition).normalized;

        if (showCollider)
        {
            Gizmos.color = new Color(.6f, 1, .51f, .6f);
            Gizmos.DrawWireSphere(position, colliderRadius);
        }

        if (m_boardMesh == null)
        {
            var vertices = new Vector3[4];
            var uv = new Vector2[4];
            var triangles = new int[6];

            vertices[0] = new Vector3(0, 0, .33f);
            vertices[1] = new Vector3(.85f, 0, .33f);
            vertices[2] = new Vector3(0, 0, 0);
            vertices[3] = new Vector3(.85f, 0, 0);

            uv[0] = new Vector2(0, 1);
            uv[1] = new Vector2(1, 1);
            uv[2] = new Vector2(0, 0);
            uv[3] = new Vector2(1, 0);

            triangles[0] = 0;
            triangles[1] = 1;
            triangles[2] = 2;
            triangles[3] = 2;
            triangles[4] = 1;
            triangles[5] = 3;

            m_boardMesh = new Mesh {vertices = vertices, uv = uv, triangles = triangles};
            m_boardMesh.RecalculateNormals();
        }


        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(offsetPosition, offsetDestination);

        Gizmos.color = Color.yellow;

        var rotation = Quaternion.LookRotation(direction);
        for (var i = 0; i < _boardsNeeded; i++)
        {
            var offset = direction * BOARD_DISTANCE * i;
            var newPosition = new Vector3(offsetPosition.x + offset.x, 0, offsetPosition.z + offset.z + .33f);
            Gizmos.DrawMesh(m_boardMesh, newPosition, rotation);
        }

        Handles.color = Color.cyan;
        Handles.DrawWireDisc(m_destination, Vector3.up, _destinationRadius);
        Handles.DrawWireDisc(position, Vector3.up, colliderRadius + MAX_BOT_DISTANCE);

        Handles.color = new Color(0, 1, 1, .1f);
        Handles.DrawSolidDisc(m_destination, Vector3.up, _destinationRadius);
        Handles.DrawSolidDisc(position, Vector3.up, colliderRadius + MAX_BOT_DISTANCE);
    }
#endif
}