﻿using MightyAttributes;
using UnityEngine;
using UnityEngine.Serialization;

public class BoardBehaviour : BaseTargetBehaviour
{
    // @formatter:off
    
    [Components] [SerializeField] private GameObject _boardGroupObject;
    [Components] [SerializeField, GetComponentInChildren, ReadOnly] private MeshRenderer _renderer;
    [Components] [SerializeField, GetComponent, ReadOnly] private Collider _collider;
    
    // @formatter:on

    private Transform m_transform;
    
    public Vector3 Position { get; private set; }
    public int BoardGroupID { get; private set; }

    public override void Init()
    {
        if (!IsInit)
        {
            m_transform = transform;
            Position = m_transform.position;
            
            BoardGroupID = _boardGroupObject.GetInstanceID();
        }

        base.Init();

        SetLayer(StaticLayers.COLLECTIBLE_BOARD_LAYER);
        EnableCollider(true);
        SetAsTrigger(true);
    }

    public void SetColor(Color boardColor) => _renderer.material.color = boardColor;
    
    public void MoveTo(Transform parent, Vector3 position, Quaternion rotation)
    {
        m_transform.parent = parent;
        m_transform.localPosition = position;
        m_transform.rotation = rotation;
    }
    
    public void EnableCollider(bool value) => _collider.enabled = value;

    public void SetAsTrigger(bool value) => _collider.isTrigger = value;

    public void SetLayer(int layer) => gameObject.layer = layer;

}