﻿using UnityEngine;

public class BaseTargetBehaviour : BaseBehaviour
{
    [Components] [SerializeField] private GameObject _objectToDetect;
    
    public int GameObjectID { get; private set; }

    public override void Init()
    {
        if (!IsInit) GameObjectID = _objectToDetect.GetInstanceID();

        base.Init();
    }
}
