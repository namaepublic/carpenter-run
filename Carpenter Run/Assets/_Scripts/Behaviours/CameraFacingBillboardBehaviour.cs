﻿using UnityEngine;

public class CameraFacingBillboardBehaviour : BaseBehaviour
{
    private Transform m_transform;
    private CameraBehaviour m_cameraBehaviour;

    public override void Init()
    {
        if (!IsInit)
        {
            m_transform = transform;
            m_cameraBehaviour = InstanceManager.CameraBehaviour;
        }

        base.Init();

        ApplyRotation();
    }

    public void UpdateBehaviour()
    {
        if (!IsInit || !IsPlaying) return;
        ApplyRotation();
    }

    private void ApplyRotation()
    {
        var (forward, up) = m_cameraBehaviour.CameraAxis;
        m_transform.rotation = Quaternion.LookRotation(forward, up);
    }
}