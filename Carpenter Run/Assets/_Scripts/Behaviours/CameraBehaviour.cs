﻿using System;
using Cinemachine;
using MightyAttributes;
using UnityEngine;

public class CameraBehaviour : BaseBehaviour
{
    private const float HEIGHT_TOLERANCE = 0.01f;

    [Components] [SerializeField, GetComponentInChildren, ReadOnly]
    private CinemachineFreeLook _cinemachineCamera;

    [Components] [SerializeField, GetComponentInChildrenWithTag("MainCamera"), ReadOnly]
    private Transform _cameraTransform;

    [SerializeField, PercentSlider(true)] private float _heightSmoothingFactor;

    private float m_targettedHeight;
    private bool m_heightHardset;
    private float m_smoothingTime;
    
    public (Vector3 forward, Vector3 up) CameraAxis => (_cameraTransform.forward, _cameraTransform.up);

    public override void Init()
    {
        base.Init();
        HardsetHeightOnce(0);
        m_targettedHeight = 0;
        m_smoothingTime = 1 - _heightSmoothingFactor;
    }

    public override void Play()
    {
        base.Play();
        TargetHeight(.5f);
    }

    public void FixedUpdateBehaviour()
    {
        if (!IsInit || !IsPlaying) return;

        if (m_heightHardset) return;
        
        var height = _cinemachineCamera.m_YAxis.Value;

        if (Math.Abs(height - m_targettedHeight) > HEIGHT_TOLERANCE)
            _cinemachineCamera.m_YAxis.Value = Mathf.Lerp(height, m_targettedHeight, m_smoothingTime);
        else
            HardsetHeightOnce(m_targettedHeight);
    }

    public Vector3 GetCameraGroundPosition()
    {
        var cameraPosition = _cameraTransform.position;
        return new Vector3(cameraPosition.x, InstanceManager.LevelManager.StartY, cameraPosition.z);
    }

    public void LockRotationToTarget(bool value)
    {
        _cinemachineCamera.m_BindingMode = value
            ? CinemachineTransposer.BindingMode.LockToTarget
            : CinemachineTransposer.BindingMode.SimpleFollowWithWorldUp;
    }

    public void TargetHeight(float height)
    {
        m_heightHardset = false;
        m_targettedHeight = height;
    }

    public void HardsetHeightOnce(float height)
    {
        if (m_heightHardset) return;

        _cinemachineCamera.m_YAxis.Value = height;
        m_heightHardset = true;
    }
}