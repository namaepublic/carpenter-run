﻿using MightyAttributes.Utilities;
using UnityEngine;

public class FinishLineBehaviour : BaseBehaviour
{
    [SerializeField] private Vector3 _destinationOffset;
    [SerializeField] private Vector2 _destinationRange;

    public Vector3 Position { get; private set; }

    public override void Init()
    {
        if (!IsInit) Position = transform.position + _destinationOffset;

        base.Init();
    }

    public Vector3 GetDestination()
    {
        var destinationCircle = new Vector3(Random.Range(-1, 1f) * _destinationRange.x, 0, Random.Range(-1, 1f) * _destinationRange.y);
        return new Vector3(destinationCircle.x + Position.x, 0, destinationCircle.y + Position.z);
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        var tfm = transform;
        Position = tfm.position + _destinationOffset;
        
        GizmosUtilities.DrawTopDownRectangle(Position, tfm.rotation, _destinationRange, new Color(0, 1, 1, .1f), Color.cyan);
    }
#endif
}