﻿using MightyAttributes;
using UnityEngine;

#if UNITY_EDITOR
[FoldBox("Components")]
#endif
public class ComponentsAttribute : BaseWrapperAttribute
{
}

#if UNITY_EDITOR
[FoldBox("Animations")]
#endif
public class AnimationsAttribute : BaseWrapperAttribute
{
}

public abstract class BaseBehaviour : MonoBehaviour
{
    public bool IsInit { get; private set; }
    public bool IsPlaying { get; private set; }

    public virtual void Init() => IsInit = true;
    public virtual void Reset() => IsInit = false;
    
    public virtual void Play() => IsPlaying = true;
    public virtual void Stop() => IsPlaying = false;
}
