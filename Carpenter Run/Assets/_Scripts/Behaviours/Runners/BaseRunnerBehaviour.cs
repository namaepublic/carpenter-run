﻿using MightyAttributes;
using MightyAttributes.Utilities;
using TMPro;
using UnityEngine;

#if UNITY_EDITOR
[Box("Identity")]
#endif
public class IdentityAttribute : BaseWrapperAttribute
{
}

#if UNITY_EDITOR
[Box("Movement")]
#endif
public class MovementAttribute : BaseWrapperAttribute
{
}

#if UNITY_EDITOR
[Box("Detection")]
#endif
public class DetectionAttribute : BaseWrapperAttribute
{
}

public abstract class BaseRunnerBehaviour : BaseBehaviour
{
    protected enum TrackState : byte
    {
        OverTrack,
        OverBoards,
        MidAir,
    }

    protected enum MoveState : byte
    {
        Running,
        Jumping,
        Climbing,
        Falling,
        LookingAtCamera
    }

    protected enum BoostState : byte
    {
        Standby,
        Up,
        Down
    }

    private const float MIN_HEIGHT = -1.5f;
    private const int MAX_TRACK_DETECTION = 10;

    private const float BOARD_Y = -.05f;
    private const float BOARD_Z_OFFSET = .15f;

    private const float ANIMATION_SPEED_FACTOR = .3f;
    private const float BOOST_DOWN_FACTOR = 1.33f;

    // @formatter:off
    
    [Identity] [SerializeField] private string _name;
    
    [Components] [SerializeField, GetComponent, ReadOnly] 
    protected Rigidbody _rigidbody;
    [Components] [SerializeField, GetComponentInChildren, ReadOnly] 
    private Animator _animator;
    [Components] [SerializeField, GetComponentInChildren, ReadOnly]
    private BoardStackBehaviour _boardStack;   
    [Components] [SerializeField, GetComponentInChildren, ReadOnly]
    private CameraFacingBillboardBehaviour _cameraFacingBillboard;  
    [Components] [SerializeField, GetComponentInChildren, ReadOnly]
    private TextMeshProUGUI _nameText; 
    [Components] [SerializeField, GetComponentInChildrenWithTag("Graphics"), ReadOnly] 
    protected Transform _graphicsTransform;
    
    [Movement] [SerializeField] private float _runSpeed;
    [Movement] [SerializeField] private float _boostTime;
    [Movement] [SerializeField] private AnimationCurve _boostCurve;
    [Movement] [SerializeField] private float _jumpTime;
    [Movement] [SerializeField] private AnimationCurve _jumpCurve;
    [Movement] [SerializeField, PercentSlider(true)] protected float _rotationSmoothingFactor;

    [Detection] [SerializeField] private float _trackDetectionRadius;
    [Detection] [SerializeField] private Vector3 _trackDetectionOffset;
    [Detection] [SerializeField] private Vector3 _climbRayOrigin;
    [Detection] [SerializeField] private Vector3 _climbRayDestination;

    [Animations] [SerializeField, AnimatorParameter("Speed"), ReadOnly] private int _speedID;
    [Animations] [SerializeField, AnimatorParameter("Run"), ReadOnly] private int _runID;     
    [Animations] [SerializeField, AnimatorParameter("Jump"), ReadOnly] private int _jumpID;     
    [Animations] [SerializeField, AnimatorParameter("Land"), ReadOnly] private int _landID; 
    [Animations] [SerializeField, AnimatorParameter("Fall"), ReadOnly] private int _fallID; 
    
    // @formatter:on

    [Title("Debug")] [ShowNonSerialized] private float m_jumpTimer;
    [ShowNonSerialized] private float m_boostTimer;

    private readonly Collider[] m_trackDetectedColliders = new Collider[MAX_TRACK_DETECTION];

    protected LevelManager m_levelManager;
    private float m_rotationSmoothingSpeed;

    private BoostState m_boostState;

    protected TrackState CurrentTrackState { get; private set; }
    protected MoveState CurrentMoveState { get; private set; }

    protected Transform Transform { get; private set; }
    public Vector3 Position => _rigidbody.position;

    public BoardStackBehaviour BoardStack => _boardStack;

    public string Name
    {
        get => _name;
        set
        {
            _name = value;
            _nameText.text = value;
        }
    }

    public override void Init()
    {
        if (!IsInit)
        {
            Transform = transform;

            gameObject.SetActive(true);

            m_rotationSmoothingSpeed = 1 - _rotationSmoothingFactor;
        }

        _nameText.text = Name;
        _nameText.color = _boardStack.BoardColor;

        base.Init();

        m_levelManager = InstanceManager.LevelManager;

        Restart();
    }

    public override void Reset()
    {
        if (IsInit) gameObject.SetActive(false);

        base.Reset();
    }

    public override void Play()
    {
        base.Play();

        Restart();

        _animator.SetTrigger(_runID);
    }

    public override void Stop()
    {
        base.Stop();

        _boardStack.Init();
        _animator.Rebind();

        _rigidbody.velocity = Vector3.zero;
    }

    public void SetPositionAndRotation(Vector3 position, Quaternion rotation)
    {
        Transform.position = position;
        Transform.rotation = rotation;
    }

    private void Restart()
    {
        CurrentTrackState = TrackState.OverTrack;
        CurrentMoveState = MoveState.Running;

        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
        m_jumpTimer = m_boostTimer = 0;

        _boardStack.Init();

        _cameraFacingBillboard.Init();
        _cameraFacingBillboard.Play();

        _animator.Rebind();

        _rigidbody.velocity = Vector3.zero;
    }

    public void UpdateBehaviour()
    {
        if (!IsInit) return;
        _cameraFacingBillboard.UpdateBehaviour();

        if (!IsPlaying) return;
        OnUpdate();
    }

    public void FixedUpdateBehaviour()
    {
        if (CurrentMoveState == MoveState.LookingAtCamera)
            RotateForwardTowards((InstanceManager.CameraBehaviour.GetCameraGroundPosition() - Position).normalized);

        if (!IsInit || !IsPlaying) return;

        var currentPosition = Position;
        if (currentPosition.y < MIN_HEIGHT)
        {
            _animator.SetTrigger(_fallID);
            OnFallOutOfMap();
            Stop();
            return;
        }

        CurrentTrackState = TrackState.MidAir;
        m_boostState = m_boostState != BoostState.Standby ? BoostState.Down : BoostState.Standby;

        if (CurrentMoveState == MoveState.Falling)
        {
            if (CanClimb(currentPosition, out var detected))
            {
                Climb(currentPosition);
                ManageDetectionTrack(detected, currentPosition);
                if (!IsPlaying) return;
            }
        }
        else
        {
            if (TryToDetectTrack(currentPosition, out var detectedCount))
                for (var i = 0; i < detectedCount; i++)
                {
                    ManageDetectionTrack(m_trackDetectedColliders[i].gameObject, currentPosition);
                    if (!IsPlaying) return;
                }
        }

        ManageTrackState(currentPosition);
        ManageMovement(currentPosition);

        OnFixedUpdate(currentPosition);
    }

    private bool TryToDetectTrack(Vector3 currentPosition, out int detectedCount)
    {
        detectedCount = Physics.OverlapSphereNonAlloc(GetTrackDetectionPosition(currentPosition), _trackDetectionRadius,
            m_trackDetectedColliders, StaticLayers.TRACK_DETECTION_MASK);
        return detectedCount > 0;
    }

    private void ManageDetectionTrack(GameObject detected, Vector3 currentPosition)
    {
        var layer = detected.layer;
        switch (layer)
        {
            case StaticLayers.TRACK_LAYER:
                if (CurrentTrackState != TrackState.OverBoards)
                    CurrentTrackState = TrackState.OverTrack;
                break;

            case StaticLayers.WALKABLE_BOARD_LAYER:
                CurrentTrackState = TrackState.OverBoards;
                break;

            case StaticLayers.COLLECTIBLE_BOARD_LAYER:
                if (CurrentMoveState == MoveState.Running || CurrentMoveState == MoveState.Climbing)
                {
                    if (m_levelManager.TryGetBoard(detected.GetInstanceID(), out var board))
                    {
                        _boardStack.AddBoard(board);
                        OnBoardAmountModified();
                    }
                }

                break;

            case StaticLayers.FINISH_LINE_LAYER:
                var boardAmount = _boardStack.Count;
                Stop();
                CurrentMoveState = MoveState.LookingAtCamera;
                InstanceManager.LevelManager.Rank(this, boardAmount);
                OnFinishLineCrossed();
                break;
        }

        OnObjectDetected(detected, currentPosition);
    }

    private void ManageTrackState(Vector3 currentPosition)
    {
        switch (CurrentTrackState)
        {
            case TrackState.OverTrack:
                if (CurrentMoveState == MoveState.Jumping) break;

                CurrentMoveState = MoveState.Running;
                break;

            case TrackState.OverBoards:
                if (CurrentMoveState == MoveState.Jumping) break;

                CurrentMoveState = MoveState.Running;
                m_boostState = BoostState.Up;

                break;
            case TrackState.MidAir:
                if (CurrentMoveState != MoveState.Running && CurrentMoveState != MoveState.Climbing) break;

                if (!_boardStack.IsEmpty())
                    Board(currentPosition);
                else
                    Jump();

                break;
        }
    }

    private void ManageMovement(Vector3 currentPosition)
    {
        switch (CurrentMoveState)
        {
            case MoveState.Jumping:
            {
                _graphicsTransform.localPosition = new Vector3(0, _jumpCurve.Evaluate(m_jumpTimer / _jumpTime), 0);
                m_jumpTimer += Time.deltaTime;
                if (m_jumpTimer > _jumpTime) Fall();

                break;
            }
        }

        switch (m_boostState)
        {
            case BoostState.Standby:
                m_boostTimer = 0;
                break;
            case BoostState.Up:
                m_boostTimer += Time.deltaTime;
                if (m_boostTimer > _boostTime)
                    m_boostTimer = _boostTime;

                break;
            case BoostState.Down:
                m_boostTimer -= Time.deltaTime * BOOST_DOWN_FACTOR;
                if (m_boostTimer <= 0)
                {
                    m_boostTimer = 0;
                    m_boostState = BoostState.Standby;
                }

                break;
        }

        var speed = GetMoveSpeed();

        if (CurrentMoveState == MoveState.Running)
            _animator.SetFloat(_speedID, speed * ANIMATION_SPEED_FACTOR);

        Move(currentPosition, speed);
    }

    private void Board(Vector3 currentPosition)
    {
        if (!_boardStack.TryRemoveBoard(out var board)) return;

        CurrentMoveState = MoveState.Running;
        m_boostState = BoostState.Up;

        var boardPosition = new Vector3(currentPosition.x, BOARD_Y, currentPosition.z + Transform.forward.z * BOARD_Z_OFFSET);
        board.MoveTo(m_levelManager.PlacedBoardsTransform, boardPosition, Transform.rotation);

        board.SetLayer(StaticLayers.WALKABLE_BOARD_LAYER);
        board.SetAsTrigger(false);
        board.EnableCollider(true);

        OnBoardAmountModified();
    }

    private void Jump()
    {
        CurrentMoveState = MoveState.Jumping;
        _animator.SetTrigger(_jumpID);
    }

    private void Fall()
    {
        if (CurrentMoveState != MoveState.Jumping) return;

        CurrentMoveState = MoveState.Falling;
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    private bool CanClimb(Vector3 currentPosition, out GameObject detected)
    {
        if (Physics.Raycast(GetClimbRay(currentPosition, out var length), out var hit, length, StaticLayers.CLIMB_DETECTION_MASK))
        {
            detected = hit.transform.gameObject;
            return true;
        }

        detected = null;
        return false;
    }

    private void Climb(Vector3 currentPosition)
    {
        if (CurrentMoveState != MoveState.Climbing)
            _animator.SetTrigger(_landID);

        CurrentMoveState = MoveState.Climbing;

        m_jumpTimer = 0;

        Transform.position = new Vector3(currentPosition.x, m_levelManager.StartY, currentPosition.z);

        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionY;
    }

    private Ray GetClimbRay(Vector3 currentPosition, out float length)
    {
        var rotation = Transform.rotation;
        var startY = m_levelManager.StartY;

        var rotatedOrigin = rotation * _climbRayOrigin;
        var origin = new Vector3(currentPosition.x + rotatedOrigin.x, startY + rotatedOrigin.y, currentPosition.z + rotatedOrigin.z);

        var rotatedDestination = rotation * _climbRayDestination;
        var destination = new Vector3(currentPosition.x + rotatedDestination.x, startY + rotatedDestination.y,
            currentPosition.z + rotatedDestination.z);

        var direction = destination - origin;
        length = VectorUtilities.Magnitude(direction);
        return new Ray(origin, direction.normalized);
    }

    private Vector3 GetTrackDetectionPosition(Vector3 currentPosition) => OffsetAndRotatePosition(currentPosition, _trackDetectionOffset);
    protected Vector3 OffsetAndRotatePosition(Vector3 currentPosition, Vector3 offset) => currentPosition + Transform.rotation * offset;

    private float GetMoveSpeed() => _runSpeed + _boostCurve.Evaluate(m_boostTimer / _boostTime);

    protected Vector3 RotateForwardTowards(Vector3 direction)
    {
        var forward = Transform.forward;
        forward = Vector3.Lerp(forward, direction, m_rotationSmoothingSpeed);
        Transform.forward = forward;

        return forward;
    }

    protected abstract void OnUpdate();
    protected abstract void OnFixedUpdate(Vector3 currentPosition);

    protected abstract void Move(Vector3 currentPosition, float speed);

    protected abstract void OnBoardAmountModified();
    protected abstract void OnObjectDetected(GameObject detected, Vector3 currentPosition);

    protected abstract void OnFallOutOfMap();
    protected abstract void OnFinishLineCrossed();

#if UNITY_EDITOR
    protected virtual void OnDrawGizmosSelected()
    {
        m_levelManager = InstanceManager.LevelManager;
        if (!m_levelManager) return;

        Transform = transform;
        var currentPosition = Position;

        Gizmos.color = Color.red;
        var ray = GetClimbRay(currentPosition, out var length);
        Gizmos.DrawRay(ray.origin, ray.direction * length);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(GetTrackDetectionPosition(currentPosition), _trackDetectionRadius);

        var size = 10;
        var position = Transform.position;

        GizmosUtilities.DrawTopDownRectangle(new Vector3(position.x, MIN_HEIGHT, position.z), Quaternion.identity, new Vector2(size, size),
            new Color(1, 1, 0, .05f), new Color(1, 1, 0));
    }
#endif
}