﻿using MightyAttributes;
using UnityEngine;

public class PlayerBehaviour : BaseRunnerBehaviour
{
    // @formatter:off
    
    [Movement] [SerializeField] protected float _rotationSpeed;
    [Detection] [SerializeField, MinValue(1)] private uint _maxDezoomBoardAmount;

    // @formatter:on

    private float m_previousMouseX;

    private bool m_shouldRotate;
    private float m_rotateAmount;

    public override void Play()
    {
        base.Play();
        m_previousMouseX = 0;
    }

    protected override void OnUpdate()
    {
        if (!IsInit || !IsPlaying) return;

        var currentMouseX = Input.mousePosition.x;
        m_rotateAmount = currentMouseX - m_previousMouseX;
        m_previousMouseX = currentMouseX;

        m_shouldRotate = Input.GetMouseButton(0);
    }

    protected override void OnFixedUpdate(Vector3 currentPosition) => InstanceManager.LevelManager.RefreshPlayerRank();

    protected override void Move(Vector3 currentPosition, float speed)
    {
        if (m_shouldRotate)
            Transform.Rotate(Vector3.up, m_rotateAmount * _rotationSpeed * Time.deltaTime);

        var horizontalVelocity = Transform.forward * speed;
        _rigidbody.velocity = new Vector3(horizontalVelocity.x, _rigidbody.velocity.y, horizontalVelocity.z);
    }

    protected override void OnBoardAmountModified() =>
        InstanceManager.CameraBehaviour.TargetHeight(.5f + Mathf.Min((float) BoardStack.Count / _maxDezoomBoardAmount, 1) / 2);

    protected override void OnObjectDetected(GameObject detected, Vector3 currentPosition)
    {
    }

    protected override void OnFallOutOfMap() => InstanceManager.GameManager.RestartLevel();
    
    protected override void OnFinishLineCrossed()
    {
        InstanceManager.LevelManager.EndLevel();
        InstanceManager.CameraBehaviour.LockRotationToTarget(false);
        InstanceManager.CameraBehaviour.TargetHeight(0);
    }
}