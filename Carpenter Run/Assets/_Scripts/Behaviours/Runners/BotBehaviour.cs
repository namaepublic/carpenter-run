﻿using System.Collections.Generic;
using MightyAttributes;
using MightyAttributes.Utilities;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.AI;

#if UNITY_EDITOR
[Box("Skills")]
#endif
public class SkillsAttribute : BaseWrapperAttribute
{
}


public class BotBehaviour : BaseRunnerBehaviour
{
    private enum TargetType : byte
    {
        FinishLine,
        Shortcut,
        Board,
    }

    private const int MAX_WAYPOINTS = 100;
    private const float MIN_WAYPOINT_DISTANCE = .5f;
    private const float DESTINATION_TOLERANCE = .1f;

    private const int MAX_TARGET_AMOUNT = 3;

    // @formatter:off

    [Detection] [SerializeField] private Vector3 _targetDetectionOffset;
    [Detection] [SerializeField] private float _targetDetectionRadius;
    
    [Skills] [SerializeField, PercentSlider] private int _chanceToCrossShortcut;
    [Skills] [SerializeField, PercentSlider] private int _chanceToCollectBoards;

    // @formatter:on

    private readonly Vector3[] m_waypoints = new Vector3[MAX_WAYPOINTS];
    private readonly Collider[] m_targetDetectionColliders = new Collider[MAX_TARGET_AMOUNT];

    private readonly Dictionary<int, bool> m_boardCollectedByID = new Dictionary<int, bool>();
    private readonly Dictionary<int, bool> m_shortcutCrossedByID = new Dictionary<int, bool>();

    private NavMeshPath m_path;
    private int m_pathArea;

    private Vector3 m_finishLineDestination;
    private Vector3 m_currentDestination;
    private int m_waypointsIndex, m_waypointsCount;

    private TargetType m_targetType;

    private BoardBehaviour m_targetedBoard;
    private bool m_shortcutting;

    public override void Init()
    {
        if (!IsInit)
        {
            m_path = new NavMeshPath();
            m_pathArea = 1 << 0;
        }

        base.Init();
    }

    public override void Play()
    {
        base.Play();

        m_shortcutCrossedByID.Clear();
        m_boardCollectedByID.Clear();

        m_finishLineDestination = InstanceManager.LevelManager.GetFinishLineDestination();
        TargetFinishLine(Position);
    }

    protected override void OnUpdate()
    {
    }

    protected override void OnFixedUpdate(Vector3 currentPosition)
    {
        if (!IsInit || !IsPlaying) return;

        if (m_targetType == TargetType.Shortcut) return;

        if (CurrentTrackState != TrackState.OverTrack || CurrentMoveState != MoveState.Running) return;

        if (TryToDetectTarget(currentPosition, out var detectedCount))
            for (var i = 0; i < detectedCount; i++)
            {
                ManageDetectedTarget(m_targetDetectionColliders[i].gameObject, currentPosition);
                if (m_targetType == TargetType.Shortcut) break;
            }

        if (m_targetType != TargetType.FinishLine && VectorUtilities.SqrDistance(Position, m_currentDestination) < DESTINATION_TOLERANCE)
            TargetFinishLine(currentPosition);
    }

    private bool TryToDetectTarget(Vector3 currentPosition, out int detectedCount)
    {
        detectedCount = Physics.OverlapSphereNonAlloc(GetTargetDetectionPosition(currentPosition), _targetDetectionRadius,
            m_targetDetectionColliders, StaticLayers.TARGET_DETECTION_MASK);
        return detectedCount > 0;
    }

    private void ManageDetectedTarget(GameObject detected, Vector3 currentPosition)
    {
        Vector3 destination;
        var id = detected.GetInstanceID();

        var layer = detected.layer;
        switch (layer)
        {
            case StaticLayers.SHORTCUT_LAYER when m_shortcutCrossedByID.ContainsKey(id):
                break;
            case StaticLayers.SHORTCUT_LAYER:
                if (m_levelManager.TryGetShortcutDestination(id, BoardStack.Count, _chanceToCrossShortcut, out destination))
                {
                    m_targetType = TargetType.Shortcut;
                    m_currentDestination = destination;
                    m_shortcutCrossedByID[id] = true;
                }
                else
                    m_shortcutCrossedByID[id] = false;

                break;

            case StaticLayers.COLLECTIBLE_BOARD_LAYER when m_targetType != TargetType.FinishLine:
                break;
            case StaticLayers.COLLECTIBLE_BOARD_LAYER:
                if (m_levelManager.TryGetBoardDestination(id, BoardStack.Count, _chanceToCollectBoards, out m_targetedBoard,
                    out var boardGroupID, out destination))
                {
                    if (m_boardCollectedByID.ContainsKey(boardGroupID)) return;

                    m_targetType = TargetType.Board;
                    m_currentDestination = destination;
                    m_boardCollectedByID[boardGroupID] = true;
                }
                else
                    m_boardCollectedByID[boardGroupID] = false;

                break;
        }
    }

    protected override void Move(Vector3 currentPosition, float speed)
    {
        var direction = GetDirection(currentPosition);

        var forward = RotateForwardTowards(direction);
        var horizontalVelocity = forward * speed;
        _rigidbody.velocity = new Vector3(horizontalVelocity.x, _rigidbody.velocity.y, horizontalVelocity.z);
    }

    private void TargetFinishLine(Vector3 currentPosition) => SetTarget(currentPosition, m_finishLineDestination, TargetType.FinishLine);

    private void SetTarget(Vector3 currentPosition, Vector3 destination, TargetType targetType)
    {
        if (!TryCalculatePath(currentPosition, destination)) return;

        m_targetType = targetType;
        m_currentDestination = destination;
        m_shortcutting = false;
    }

    private bool TryCalculatePath(Vector3 currentPosition, Vector3 destination)
    {
        NavMesh.CalculatePath(currentPosition, destination, m_pathArea, m_path);

        m_waypointsCount = m_path.GetCornersNonAlloc(m_waypoints);
        m_waypointsIndex = 0;

        return m_waypointsCount > 0;
    }

    private Vector3 GetDirection(Vector3 currentPosition)
    {
        switch (m_targetType)
        {
            case TargetType.FinishLine:
                return GetFinishLineDirection(currentPosition);

            case TargetType.Shortcut:
                return (m_currentDestination - currentPosition).normalized;

            case TargetType.Board:
                if (m_levelManager.IsBoardGroupAvailable(m_targetedBoard.BoardGroupID))
                    return (m_currentDestination - currentPosition).normalized;

                TargetFinishLine(currentPosition);
                return GetFinishLineDirection(currentPosition);
        }

        return Vector3.zero;
    }

    private Vector3 GetFinishLineDirection(Vector3 currentPosition)
    {
        if (m_waypointsIndex >= m_waypointsCount - 1) return Vector3.zero;

        var waypoint = m_waypoints[m_waypointsIndex + 1];
        var nextPoint = new Vector3(waypoint.x, 0, waypoint.z);

        var delta = nextPoint - currentPosition;

        if (VectorUtilities.SqrMagnitude(delta) < MIN_WAYPOINT_DISTANCE)
            m_waypointsIndex++;

        return delta.normalized;
    }

    private Vector3 GetTargetDetectionPosition(Vector3 currentPosition) => OffsetAndRotatePosition(currentPosition, _targetDetectionOffset);

    protected override void OnBoardAmountModified()
    {
    }

    protected override void OnObjectDetected(GameObject detected, Vector3 currentPosition)
    {
        switch (m_targetType)
        {
            case TargetType.Board:
                if (detected.GetInstanceID() == m_targetedBoard.GameObjectID)
                    TargetFinishLine(currentPosition);

                break;

            case TargetType.Shortcut:
                if (CurrentMoveState != MoveState.Running) return;

                switch (CurrentTrackState)
                {
                    case TrackState.OverBoards:
                        m_shortcutting = true;
                        break;
                    case TrackState.OverTrack when m_shortcutting:
                        TargetFinishLine(currentPosition);
                        break;
                }

                break;
        }
    }

    protected override void OnFallOutOfMap()
    {
    }

    protected override void OnFinishLineCrossed()
    {
    }

#if UNITY_EDITOR
    protected override void OnDrawGizmosSelected()
    {
        if (!m_levelManager) return;
        
        base.OnDrawGizmosSelected();
        var currentPosition = Position;

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(GetTargetDetectionPosition(currentPosition), _targetDetectionRadius);

        if (!EditorApplication.isPlaying)
        {
            if (!InstanceManager.LevelManager) return;
            InstanceManager.LevelManager.InitStartAndFinishLinePositions();
            m_path = new NavMeshPath();
            m_pathArea = 1 << 0;

            TargetFinishLine(currentPosition);
        }

        for (var i = 0; i < m_waypointsCount - 1; i++)
            Debug.DrawLine(m_waypoints[i], m_waypoints[i + 1], Color.magenta);
    }
#endif
}