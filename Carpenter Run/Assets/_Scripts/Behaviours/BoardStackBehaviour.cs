﻿using System.Collections.Generic;
using UnityEngine;

public class BoardStackBehaviour : BaseBehaviour
{
    [SerializeField] private float _boardHeight;
    [SerializeField] private Color _boardColor;

    private Transform m_transform;
    private readonly Stack<BoardBehaviour> m_boards = new Stack<BoardBehaviour>();

    public ushort Count => (ushort) m_boards.Count;
    public Color BoardColor => _boardColor;

    public override void Init()
    {
        if (!IsInit) m_transform = transform;
        base.Init();
        
        Clear();
    }

    public bool IsEmpty() => Count == 0;
    
    public void AddBoard(BoardBehaviour board)
    {
        board.SetColor(_boardColor);
        board.EnableCollider(false);
        board.MoveTo(m_transform, new Vector3(0, _boardHeight * Count, 0), m_transform.rotation);
        InstanceManager.LevelManager.CollectBoardGroup(board.BoardGroupID);
        m_boards.Push(board);
    }

    public bool TryRemoveBoard(out BoardBehaviour board)
    {
        if (!IsEmpty())
        {
            board = m_boards.Pop();
            return true;
        }

        board = null;
        return false;
    }

    public void MoveInStack(BoardStackBehaviour otherStack)
    {
        while (!IsEmpty()) 
            otherStack.AddBoard(m_boards.Pop());
    }

    public void Clear()
    {
        while (!IsEmpty()) 
            Destroy(m_boards.Pop().gameObject);
    }
}
