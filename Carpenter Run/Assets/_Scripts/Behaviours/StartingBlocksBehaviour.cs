﻿using System.Linq;
using MightyAttributes;
using MightyAttributes.Utilities;
using UnityEditor;
using UnityEngine;

public class StartingBlocksBehaviour : BaseBehaviour
{
    [SerializeField] private float _width;
    [SerializeField] private float _spaceBetweenRunners;

    [SerializeField, ReadOnly] private Transform[] _blocks;

    public Quaternion Rotation { get; private set; }
    public float YPosition { get; private set; }

    public override void Init()
    {
        if (!IsInit)
        {
            var tfm = transform;
            YPosition = tfm.position.y;
            Rotation = tfm.rotation;
        }

        base.Init();
    }

    public Vector3 GetPositionForRunner(int runnerIndex) => runnerIndex > _blocks.Length ? Vector3.zero : _blocks[runnerIndex].position;

#if UNITY_EDITOR
    [Button]
    private void CreateBlocks()
    {
        var runnersManager = InstanceManager.RunnersManager;
        if (!runnersManager) return;

        var parent = transform;

        while (parent.childCount > 0)
            DestroyImmediate(parent.GetChild(0).gameObject);

        var count = runnersManager.RunnersCount;
        _blocks = new Transform[count];

        var right = true;
        for (var i = 0; i < count; i++)
        {
            var go = new GameObject {name = $"Block {i + 1}"};

            var child = go.transform;
            child.SetParent(parent);

            child.position = parent.position + parent.rotation * new Vector3(right ? _width / 2 : -_width / 2, 0, i * _spaceBetweenRunners);
            child.localRotation = Quaternion.identity;
            right = !right;

            _blocks[i] = child;
        }
    }

    private void OnDrawGizmos()
    {
        var runnersManager = InstanceManager.RunnersManager;
        if (!runnersManager) return;

        var selectedObjects = Selection.gameObjects;
        if (selectedObjects.Length == 0) return;

        var validObject = selectedObjects.Any(o => o == gameObject || _blocks.Contains(o.transform));

        if (!validObject) return;

        var count = runnersManager.RunnersCount;

        var tfm = transform;
        var pos = tfm.position;
        var rot = tfm.rotation;
        var semiSpace = _spaceBetweenRunners / 2;
        var length = count * semiSpace;

        GizmosUtilities.DrawTopDownRectangle(new Vector3(pos.x, pos.y, pos.z + length - semiSpace), rot, new Vector2(_width, length),
            new Color(0, 0, 0, .2f), Color.black);

        if (_blocks == null || _blocks.Length == 0) return;

        var size = new Vector2(_width / 2, semiSpace);
        foreach (var block in _blocks)
        {
            var blockPos = block.localPosition + pos;

            GizmosUtilities.DrawTopDownRectangle(new Vector3(blockPos.x, blockPos.y, blockPos.z), rot, size,
                new Color(1, 1, 1, .1f), Color.white);
        }
    }
#endif
}