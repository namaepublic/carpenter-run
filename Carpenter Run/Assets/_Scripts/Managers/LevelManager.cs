﻿using System.Collections.Generic;
using MightyAttributes;
using MightyAttributes.Utilities;
using UnityEngine;

public class LevelManager : BaseManager
{
    // @formatter:off
    
    [SerializeField] private LevelModel _levelModel;
    
    [SerializeField, FindObject, ReadOnly] private StartingBlocksBehaviour _startingBlocks;
    [SerializeField, FindObject, ReadOnly] private FinishLineBehaviour _finishLine;
    [SerializeField, FindObjectWithTag("PlacedBoards"), ReadOnly] private Transform _placedBoardsTransform;
    
    [SerializeField, FindObjects, ReadOnly] private BoardBehaviour[] _boards;
    [SerializeField, FindObjects, ReadOnly] private ShortcutBehaviour[] _shortcuts;
    
    // @formatter:on

    private readonly Dictionary<int, BoardBehaviour> m_boardsByInstanceID = new Dictionary<int, BoardBehaviour>();
    private readonly Dictionary<int, ShortcutBehaviour> m_shortcutsByInstanceID = new Dictionary<int, ShortcutBehaviour>();

    private readonly Dictionary<int, bool> m_boardGroupCollectedByID = new Dictionary<int, bool>();

    private readonly Queue<BaseRunnerBehaviour> m_leaderboard = new Queue<BaseRunnerBehaviour>();

    private RunnersManager m_runnersManager;

    public float StartY { get; private set; }
    public Quaternion StartRotation { get; private set; }

    public Transform PlacedBoardsTransform => _placedBoardsTransform;

    private void Start() => Init();

    public override void Init()
    {
        m_runnersManager = InstanceManager.RunnersManager;
        InstanceManager.LevelManager = this;

        m_boardsByInstanceID.Clear();
        m_shortcutsByInstanceID.Clear();

        m_boardGroupCollectedByID.Clear();

        m_leaderboard.Clear();

        _startingBlocks.Init();
        _finishLine.Init();

        InitStartAndFinishLinePositions();

        foreach (var board in _boards)
        {
            board.Init();
            m_boardsByInstanceID[board.GameObjectID] = board;
            m_boardGroupCollectedByID[board.BoardGroupID] = true;
        }

        foreach (var shortcut in _shortcuts)
        {
            shortcut.Init();
            m_shortcutsByInstanceID[shortcut.GameObjectID] = shortcut;
        }

        InstanceManager.GameManager.WaitForPlay();
    }

    public Vector3 GetStartPositionForRunner(int runnerIndex) => _startingBlocks.GetPositionForRunner(runnerIndex);

    public Vector3 GetFinishLineDestination() => _finishLine.GetDestination();

    public bool TryGetBoard(int boardObjectID, out BoardBehaviour board) => m_boardsByInstanceID.TryGetValue(boardObjectID, out board);

    public bool IsBoardGroupAvailable(int boardGroupID) => m_boardGroupCollectedByID[boardGroupID];
    public void CollectBoardGroup(int boardGroupID) => m_boardGroupCollectedByID[boardGroupID] = false;

    public bool TryGetBoardDestination(int boardObjectID, int boardAmount, int chance, out BoardBehaviour board, out int boardGroupID,
        out Vector3 destination)
    {
        if (TryGetBoard(boardObjectID, out board))
        {
            boardGroupID = board.BoardGroupID;
            if (IsBoardGroupAvailable(boardGroupID) && m_runnersManager.CanTargetBoard(boardAmount, chance))
            {
                destination = board.Position;
                return true;
            }
        }
        else
            boardGroupID = -1;

        destination = Vector3.zero;
        return false;
    }

    public bool TryGetShortcutDestination(int shortcutObjectID, int boardAmount, int chance, out Vector3 destination)
    {
        if (m_shortcutsByInstanceID.TryGetValue(shortcutObjectID, out var shortcut) &&
            shortcut.TryGetDestination(boardAmount, chance, out destination))
            return true;

        destination = Vector3.zero;
        return false;
    }

    public void InitStartAndFinishLinePositions()
    {
        StartY = _startingBlocks.YPosition;
        StartRotation = _startingBlocks.Rotation;
    }

    public void RefreshPlayerRank()
    {
        var runnersManager = InstanceManager.RunnersManager;
        var player = runnersManager.Player;
        var bots = runnersManager.Bots;

        var finishPosition = _finishLine.Position;
        var distanceFromFinishLine = VectorUtilities.SqrDistance(player.Position, finishPosition);
        byte rank = 1;
        foreach (var bot in bots)
            if (VectorUtilities.SqrDistance(bot.Position, finishPosition) < distanceFromFinishLine)
                rank++;

        InstanceManager.GUIManager.SetPlayerRank(rank);
    }

    public void Rank(BaseRunnerBehaviour runner, ushort boardAmount)
    {
        var coins = (ushort) (100 - m_leaderboard.Count * 20 + boardAmount);
        if (runner is PlayerBehaviour)
            InstanceManager.CoinsManager.AddAmount(coins);

        InstanceManager.GUIManager.AddItemToLeaderboard(m_leaderboard.Count, runner.Name, coins, runner.BoardStack.BoardColor);

        m_leaderboard.Enqueue(runner);
    }

    public void EndLevel() => InstanceManager.GUIManager.DisplayLeaderboard();
}