﻿using MightyAttributes;
using UnityEngine;

public class RunnersManager : BaseManager
{
    // @formatter:off

    [SerializeField, FindObject, ReadOnly] private PlayerBehaviour _player;
    [SerializeField, FindObjects, ReadOnly] private BotBehaviour[] _bots;
    
    [SerializeField] private AnimationCurve _chanceToTargetBoardByAmount;
    
    // @formatter:on

    public PlayerBehaviour Player => _player;
    public BotBehaviour[] Bots => _bots;

    public int RunnersCount => 1 + _bots.Length;

    [Button]
    public override void Init()
    {
        _chanceToTargetBoardByAmount.postWrapMode = WrapMode.Clamp;

        var levelManager = InstanceManager.LevelManager;
        var startRotation = levelManager.StartRotation;

        _player.Stop();
        _player.Init();
        _player.SetPositionAndRotation(levelManager.GetStartPositionForRunner(0), startRotation);

        for (var i = 0; i < _bots.Length; i++)
        {
            var runner = _bots[i];
            runner.Stop();
            runner.Init();
            runner.SetPositionAndRotation(levelManager.GetStartPositionForRunner(i + 1), startRotation);
        }
    }

    public void Play()
    {
        _player.Play();
        foreach (var bot in _bots)
            bot.Play();
    }

    public void UpdateManager()
    {
        _player.UpdateBehaviour();
        foreach (var bot in _bots)
            bot.UpdateBehaviour();
    }

    public void FixedUpdateManager()
    {
        _player.FixedUpdateBehaviour();
        foreach (var bot in _bots)
            bot.FixedUpdateBehaviour();
    }

    public void SetPlayerName(string playerName) => _player.Name = playerName;
    public void SavePlayerName() => SavedDataServices.PlayerName = _player.Name;

    public bool CanTargetBoard(int boardAmount, int chance) => 
        Random.Range(0, 101) <= chance && Random.Range(0, 101) <= _chanceToTargetBoardByAmount.Evaluate(boardAmount);
}