﻿using UnityEngine;

public class CoinsManager : BaseManager
{
    [SerializeField] private UIntEvent _onAmountChanges;

    public override void Init() => _onAmountChanges.Invoke(SavedDataServices.CoinsAmount);

    public void AddAmount(uint amount) => _onAmountChanges.Invoke(SavedDataServices.CoinsAmount += amount);

    public bool TryRemoveAmount(uint amount)
    {
        if (SavedDataServices.CoinsAmount < amount) return false;
        
        _onAmountChanges.Invoke(SavedDataServices.CoinsAmount -= amount);
        return true;
    }
}