﻿using MightyAttributes;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

[FindObject, ReadOnly]
public class InstanceAttribute : BaseWrapperAttribute
{
}

public class InstanceManager : BaseManager
{
    // @formatter:off
    
    [Title("Instances")] 
    [SerializeField, Instance] private GameManager _gameManager;
    [SerializeField, Instance] private GUIManager _guiManager;
    [SerializeField, Instance] private RunnersManager _runnersManager;
    [SerializeField, Instance] private CoinsManager _coinsManager;
    
    [SerializeField, Instance] private CameraBehaviour _cameraBehaviour;
    
    // @formatter:on

    private LevelManager m_levelManager;

    public static GameManager GameManager => Instance._gameManager;
    public static GUIManager GUIManager => Instance._guiManager;
    public static RunnersManager RunnersManager => Instance._runnersManager;
    public static CoinsManager CoinsManager => Instance._coinsManager;
    
    public static CameraBehaviour CameraBehaviour => Instance._cameraBehaviour;

    public static LevelManager LevelManager
    {
        get
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && Instance.m_levelManager == null)
                Instance.m_levelManager = ReferencesUtilities.FindFirstObject<LevelManager>();
#endif
            return Instance.m_levelManager;
        }
        set => Instance.m_levelManager = value;
    }

    private static InstanceManager m_instance;

    private static InstanceManager Instance
    {
        get
        {
#if UNITY_EDITOR
            if (!EditorApplication.isPlaying && m_instance == null)
                m_instance = ReferencesUtilities.FindFirstObject<InstanceManager>();
#endif
            return m_instance;
        }
    }

    public override void Init()
    {
        m_instance = this;

        _guiManager.Init();
        _coinsManager.Init();
    }
}