﻿using MightyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : BaseManager
{
    private const string ST = "st";
    private const string ND = "nd";
    private const string RD = "rd";
    private const string TH = "th";

    [SerializeField] private TextMeshProUGUI _rankText;
    [SerializeField] private TextMeshProUGUI _coinsText;
    [SerializeField] private TMP_InputField _nameInputField;
    [SerializeField] private Button _playButton;
    [SerializeField] private Button _nextButton;

    [SerializeField] private GameObject _leaderboard;

    [SerializeField, FindObjects, ReadOnly]
    private LeaderboardItem[] _leaderboardItems;

    [SerializeField] private Color[] _colorByRank;

    [Button]
    public override void Init()
    {
        ShowRankText(false);
        ShowNameField(true);
        ShowPlayButton(true);
        ShowNextButton(false);

        EnablePlayButton(false);

        UpdateCoinsText(SavedDataServices.CoinsAmount);

        ResetLeaderboard();
    }

    public void UpdateCoinsText(uint coinsAmount) => _coinsText.text = coinsAmount.ToString();

    public void ShowRankText(bool value) => _rankText.gameObject.SetActive(value);
    public void ShowNameField(bool value) => _nameInputField.gameObject.SetActive(value);
    public void ShowPlayButton(bool value) => _playButton.gameObject.SetActive(value);
    public void ShowNextButton(bool value) => _nextButton.gameObject.SetActive(value);

    public void EnablePlayButton(bool value) => _playButton.interactable = value;

    public void ResetLeaderboard()
    {
        _leaderboard.SetActive(false);

        foreach (var leaderboardItem in _leaderboardItems)
            leaderboardItem.ResetItem();
    }

    public void DisplayLeaderboard()
    {
        _leaderboard.SetActive(true);
        ShowNextButton(true);
    }

    public void AddItemToLeaderboard(int rank, string runnerName, ushort coins, Color color)
    {
        if (rank < _leaderboardItems.Length)
            _leaderboardItems[rank].SetNameAndCoins(runnerName, coins, color);
    }

    public void SetPlayerRank(byte rank)
    {
        switch (rank)
        {
            case 1:
                _rankText.text = $"{rank}{ST}";
                break;
            case 2:
                _rankText.text = $"{rank}{ND}";
                break;
            case 3:
                _rankText.text = $"{rank}{RD}";
                break;
            default:
                _rankText.text = $"{rank}{TH}";
                break;
        }

        var length = _colorByRank.Length;
        _rankText.color = rank - 1 < length ? _colorByRank[rank - 1] : _colorByRank[length - 1];
    }
}