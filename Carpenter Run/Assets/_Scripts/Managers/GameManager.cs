﻿using EncryptionTool;
using MightyAttributes;
using UnityEngine;

public class GameManager : BaseManager
{
    [SerializeField, FindObject, ReadOnly] private InstanceManager _instanceManager;
    [SerializeField, FindObject, ReadOnly] private EncryptionInitializer _encryptionInitializer;

    [SerializeField, FindAssets, Reorderable(false, options: ArrayOption.DontFold)]
    private BiomeModel[] _biomes;

    private GUIManager m_guiManager;
    private RunnersManager m_runnersManager;
    private CameraBehaviour m_cameraBehaviour;

    private void Awake() => Init();

    private void Start() => LoadLevel(SavedDataServices.BiomeIndex, SavedDataServices.LevelIndex);

    public override void Init()
    {
        SavedDataServices.Init(_encryptionInitializer);
        SavedDataServices.LoadEverythingFromLocal();

        _instanceManager.Init();

        m_runnersManager = InstanceManager.RunnersManager;
        m_cameraBehaviour = InstanceManager.CameraBehaviour;
        m_guiManager = InstanceManager.GUIManager;

        Pause();
    }

    public void WaitForPlay()
    {
        InstanceManager.GUIManager.EnablePlayButton(true);
        
        m_runnersManager.Init();
        m_cameraBehaviour.Init();
    }

    public void Play()
    {
        m_guiManager.ShowPlayButton(false);
        m_guiManager.ShowNameField(false);
        m_guiManager.ShowRankText(true);
        m_runnersManager.SavePlayerName();
        
        m_runnersManager.Play();
        m_cameraBehaviour.Play();
    }

    public void Pause()
    {
        m_guiManager.ShowRankText(false);
        m_guiManager.ShowNextButton(false);
        m_guiManager.ResetLeaderboard();
        m_guiManager.ShowPlayButton(true);
        m_guiManager.ShowNameField(true);
        
        m_guiManager.EnablePlayButton(false);
        
        InstanceManager.CameraBehaviour.LockRotationToTarget(true);
    }

    public void NextBiome()
    {
        var index = SavedDataServices.BiomeIndex;
        if (++index >= _biomes.Length)
            index = 0;

        LoadLevel(index, 0);
    }

    public BiomeModel GetBiome(int index) => _biomes[index];
    public BiomeModel GetCurrentBiome() => GetBiome(SavedDataServices.BiomeIndex);

    public void LoadLevel(byte biomeIndex, byte levelIndex, bool saveProgression = true)
    {
        if (saveProgression) SavedDataServices.BiomeIndex = biomeIndex;
        _biomes[biomeIndex].LoadLevel(levelIndex, saveProgression);
    }

    public void UnloadLevel(byte biomeIndex, byte levelIndex) => GetBiome(biomeIndex).UnloadLevel(levelIndex);

    public void NextLevel() => GetCurrentBiome().NextLevel();

    public void RestartLevel() => GetCurrentBiome().RestartLevel();

    private void Update() => m_runnersManager.UpdateManager();

    private void FixedUpdate()
    {
        m_cameraBehaviour.FixedUpdateBehaviour();
        m_runnersManager.FixedUpdateManager();
    }
}