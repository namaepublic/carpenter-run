﻿using System;
using UnityEngine.Events;

[Serializable]
public class UIntEvent : UnityEvent<uint>
{
}
